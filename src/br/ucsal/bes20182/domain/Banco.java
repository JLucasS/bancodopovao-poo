package br.ucsal.bes20182.domain;

public class Banco {

	private String CPF;

	private String nome;

	private String telefone;

	private String endereco;

	private Double renda;

	private String RG;

	private String comprovResid;

	private String comprovRend;

	private Situacao situacao;

	public Banco(String CPF, String nome, String telefone, String endereco, Double renda, Situacao situacao, String RG,
			String comprovResid, String comprovRend) {

		this.nome = nome;
		this.CPF = CPF;
		this.RG = RG;
		this.telefone = telefone;
		this.endereco = endereco;
		this.renda = renda;
		this.comprovRend = comprovRend;
		this.comprovResid = comprovResid;
		this.situacao = situacao;

	}

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String cPF) {
		CPF = cPF;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Double getRenda() {
		return renda;
	}

	public void setRenda(Double renda) {
		this.renda = renda;
	}

	public Situacao getSituacao() {
		return situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String rG) {
		RG = rG;
	}

	public String getComprovResid() {
		return comprovResid;
	}

	public void setComprovResid(String comprovResid) {
		this.comprovResid = comprovResid;
	}

	public String getComprovRend() {
		return comprovRend;
	}

	public void setComprovRend(String comprovRend) {
		this.comprovRend = comprovRend;
	}

}
