package br.ucsal.bes20182.domain;

public enum Situacao {
	ATIVA, BLOQUEADA, OCUPADA;

	public static String obterSituacao() {
		String situacaoConta = "";

		for (Situacao situacao : values()) {
			situacaoConta += situacao + ",";
		}
		return situacaoConta.substring(0, situacaoConta.length() - 1);
	}
}
