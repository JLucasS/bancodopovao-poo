package br.ucsal.bes20182.business;

import java.util.List;

import br.ucsal.bes20182.domain.Banco;
import br.ucsal.bes20182.persistence.BancoDAO;

public class BancoBO {

	public static BancoDAO bancoDAO = new BancoDAO();

	public static String incluir(Banco dados) {
		String erro = validar(dados);

		if (erro != null) {
			return erro;
		}
		return bancoDAO.incluir(dados);

	}

	public static List<Banco> obterTodos() {
		return bancoDAO.obterTodos();
	}

	private static String validar(Banco dados) {
		if (dados.getCPF().trim().isEmpty()) {
			return "ERRO: CPF n�o informado!";
		}
		if (dados.getNome().trim().isEmpty()) {
			return "ERRO: nome n�o informado!";
		}
		if (dados.getEndereco().trim().isEmpty()) {
			return "ERRO: endere�o n�o informado!";
		}
		if (dados.getTelefone().trim().isEmpty()) {
			return "ERRO: telefone n�o informado!";
		}
		if (dados.getRenda().toString().trim().isEmpty()) {
			return "ERRO: renda n�o informada!";
		}
		if (dados.getRG().trim().isEmpty()) {
			return "ERRO: RG n�o informado!";
		}
		if (dados.getComprovResid().trim().isEmpty()) {
			return "ERRO: Comprovante de residencia n�o informado!";
		}
		if (dados.getComprovRend().trim().isEmpty()) {
			return "ERRO: Comprovante de renda n�o informado!";
		}

		return null;
	}

}
