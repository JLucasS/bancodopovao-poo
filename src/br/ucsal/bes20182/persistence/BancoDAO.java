package br.ucsal.bes20182.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.domain.Banco;

public class BancoDAO {

	public List<Banco> banco = new ArrayList<>();

	public String incluir(Banco dados) {
		banco.add(dados);
		return null;
	}

	public List<Banco> obterTodos() {
		return banco;
	}

}
