package br.ucsal.bes20182.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.business.BancoBO;
import br.ucsal.bes20182.domain.Banco;
import br.ucsal.bes20182.domain.Situacao;

public class BancoTUI {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Integer cont;
		System.out.println("Informe a quantidade de contas que precisam ser criadas");
		cont = sc.nextInt();
		sc.nextLine();

		BancoTUI banco = new BancoTUI();

		for (int i = 0; i < cont; i++) {
			banco.registroCliente();
			banco.mostrar();
		}
	}

	public BancoBO bancoBO = new BancoBO();

	public void registroCliente() {
		System.out.println("BEM VINDO AO CADASTRO DO BANCO DO POV�O: ");

		String nome = obterTexto("Informe o seu nome ");
		String CPF = obterTexto("Informe seu CPF: ");
		String RG = obterTexto("Informe seu RG: ");
		String telefone = obterTexto("Informe seu telefone: ");
		String endereco = obterTexto("Informe seu endere�o: ");
		Double renda = obterInteiro("Informe sua renda m�dia: ");
		String comprovResid = obterTexto("Comprovante de residencia: ");
		String comprovRend = obterTexto("Comprovante de renda: ");
		Situacao situacao = obterSit();

		Banco banco = new Banco(CPF, nome, telefone, endereco, renda, situacao, RG, comprovResid, comprovRend);

		String erro = BancoBO.incluir(banco);
		if (erro != null) {
			System.out.println("Falha ao incluir o cliente!");
			System.out.println(erro);
		}

	}

	public void mostrar() {
		System.out.println("A conta se encontra �: ");
		List<Banco> banco = BancoBO.obterTodos();

		for (int i = 0; i < banco.size(); i++) {
			System.out.println(" \tCadastro n�mero: " + i + 1);
			System.out.println(" \tNome: " + banco.get(i).getNome());
			System.out.println(" \tCPF: " + banco.get(i).getCPF());
			System.out.println(" \tRG: " + banco.get(i).getRG());
			System.out.println(" \tTelefone: " + banco.get(i).getTelefone());
			System.out.println(" \tEndere�o: " + banco.get(i).getEndereco());
			System.out.println(" \tRenda M�dia: " + banco.get(i).getRenda());
			System.out.println(" \tComprovante de residencia apresentado: " + banco.get(i).getComprovResid());
			System.out.println(" \tComprovante de renda apresentado: " + banco.get(i).getComprovRend());
			System.out.println();

		}
	}

	private Situacao obterSit() {
		while (true) {
			System.out.println("A situa��o atual da conta � (" + Situacao.obterSituacao() + "):");
			String situacaoString = sc.nextLine();

			try {
				Situacao situacao = Situacao.valueOf(situacaoString.toUpperCase());
				return situacao;
			} catch (IllegalArgumentException e) {
				System.out.println("A situa��o informada n�o � valida!");
			}
		}
	}

	private String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return sc.nextLine();
	}

	private Double obterInteiro(String mensagem) {
		System.out.println(mensagem);
		Double valor = sc.nextDouble();
		sc.nextLine();
		return valor;
	}

}
